package product.priceservice.dao;

import java.sql.*;
import java.util.List;

public class JDBCdriver {
    private static Connection connection = null;

    private static void connectDB() {

        System.out.println("-------- PostgreSQL " + "JDBCdriver Connection Testing ------------");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your PostgreSQL JDBCdriver Driver? " + "Include in your library path!");
            e.printStackTrace();
            return;
        }

        System.out.println("PostgreSQL JDBCdriver Driver Registered!");

        try {

            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
            System.out.println(connection.toString());

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }


    //Метод для выполнения любых запросов в БД
    public static ResultSet requestDB(String req)   {
        ResultSet rs = null;
        if(connection == null) {connectDB();}
        Statement stmt;

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(req);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally { return rs;}
    }


}
