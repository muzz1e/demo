package product.priceservice.dao;

import org.springframework.stereotype.Repository;
import product.priceservice.entity.Price;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

@Repository
public class PriceDao {

    //Получаем список стоимостей продукта по id
   public ArrayList<Price> getPricesByProductId(int productId){
       ArrayList<Price> prices = new ArrayList<>();
       String sql = String.format("SELECT * FROM Prices WHERE product_id = %d;", productId);
       ResultSet rs = JDBCdriver.requestDB(sql);
       try {
       while (rs.next()){
           prices.add(new Price(productId, rs.getDouble("price")));
       }
       } catch (SQLException e) {
           e.printStackTrace();
       }

       return prices;
   }
 //Добавляем новую стоимость продукта
   public void addNewPrice(Price price){
       String sql = String.format("INSERT INTO Prices (product_id, price) VALUES ( %d, %s);", price.getProductId(), price.getPrice());
       JDBCdriver.requestDB(sql);
   }
}
