package product.priceservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import product.priceservice.entity.Price;
import product.priceservice.service.PriceServiceImpl;

@RestController
public class ControllerPrices {

    @Autowired
    PriceServiceImpl priceService;

    //Контроллер возвращает среднюю стоимость продукта по productId
    @RequestMapping(value ="/avg", method = RequestMethod.POST, produces = "application/json")
    public Double getAvgPrices(@RequestBody Price price){
        double avg = priceService.getAvgPriceByProductId(price.getProductId()).getPrice();
        return avg;
    }

    //Контроллер для добавления новой цены в базу данных
    @RequestMapping(value ="/add", method = RequestMethod.POST, produces = "application/json")
    public void addNewPrice(@RequestBody Price price){
        priceService.addNewPrice(price);
    }

}
