package product.priceservice.entity;

import javax.persistence.Entity;

@Entity
public class Price {

    private int id;
    private int productId;
    private double price;

    public Price() {
    }

    public Price(int productId) {
        this.productId = productId;
    }

    public Price(int productId, double price) {
        this.productId = productId;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public double getPrice() {
        return price;
    }
}
