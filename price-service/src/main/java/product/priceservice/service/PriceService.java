package product.priceservice.service;

import product.priceservice.entity.Price;

import java.util.ArrayList;

public interface PriceService {

    ArrayList<Price> getPricesByProductId(int productId);

    Price getAvgPriceByProductId(int productId);

    void addNewPrice(Price price);
}
