package product.priceservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.priceservice.dao.PriceDao;
import product.priceservice.entity.Price;

import java.util.ArrayList;

@Service
public class PriceServiceImpl implements PriceService {

    @Autowired
    PriceDao priceDao;

    @Override
    public ArrayList<Price> getPricesByProductId(int productId) {
        return priceDao.getPricesByProductId(productId);
    }

    @Override
    public Price getAvgPriceByProductId(int productId) {
        ArrayList<Price> prices = priceDao.getPricesByProductId(productId);
        double sum = 0;
        for (Price price : prices) {
            sum += price.getPrice();
        }
        return new Price(productId, sum/prices.size());
    }

    @Override
    public void addNewPrice(Price price) {
        priceDao.addNewPrice(price);
    }
}
