package product.common.fiegn;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient()
public interface ClientFeign {

  @RequestLine("POST")
  @Headers("Content-Type: application/json")
    double getAvgById(@Param("productId") int productId);

  @RequestLine("POST")
  @Headers("Content-Type: application/json")
  void addProduct(@Param("productId") int productId, @Param("price") double price);
}