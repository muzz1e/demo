package product.common.fiegn;

import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.stereotype.Service;

public class BuilderFeignClient {

    //Запрос Req #2
    public double getAvgByProductId(int productId){
       return Feign.builder().client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(ClientFeign.class))
                .logLevel(Logger.Level.FULL)
                .target(ClientFeign.class, "http://localhost:8081/avg").getAvgById(productId);
    }
    //Запрос Req #3
    public void addProduct(int productId, double price){
         Feign.builder().client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(ClientFeign.class))
                .logLevel(Logger.Level.FULL)
                .target(ClientFeign.class, "http://localhost:8081/add").addProduct(productId,price);
    }
}
