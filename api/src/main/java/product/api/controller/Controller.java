package product.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import product.api.entity.Product;
import product.api.service.ProductServiceImpl;
import product.common.fiegn.BuilderFeignClient;


@RestController
public class Controller {

    @Autowired
    ProductServiceImpl productService;


    BuilderFeignClient builderFeignClient = new BuilderFeignClient();

    @RequestMapping(value ="/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity addProduct(@RequestBody Product product){
        // С внешней системы приходит запрос Req#1, где есть наименование товара и цена. Ищем в таблице товар, наименование которого равно name из запроса.
        // Если товар в таблице Product нашелся, то мы отправляем запрос Req#2 сервис Price. Запрос на получение средней цены товара по product_id.
        // avg - средняя цена товара в  таблице Prices. После получения ответа мы сравниваем удовлетворяет ли условию пришедшая цена.
        // Если условие выполняется, то мы записываем цену в сервис Price с помощью запроса Req#3 и отвечаем на Req#1 status=OK.
        // Если условия не выполняется то мы не записываем цену а сразу отвечаем на Req#1 status=ERROR.
        try {
            int productId = productService.getProductIdByName(product);
            double avg = builderFeignClient.getAvgByProductId(productId);
            if(productService.validatePrice(product, avg)){
                builderFeignClient.addProduct(productId,product.getPrice());
            }
            else {
                return new ResponseEntity("ERROR",HttpStatus.BAD_REQUEST);
            }
        }
        catch (NullPointerException e){
            //Если не нашли, то создаем товар в таблице Product и отрпавляем запрос на запись цены о товаре Req#3 в сервис Price.
            System.out.println("Не нашли товар \""+ product.getName() +"\" в БД (Product)");
            productService.addNewProduct(product);
            builderFeignClient.addProduct(productService.getProductIdByName(product), product.getPrice());
        }

        return new ResponseEntity(HttpStatus.OK);
    }



}
