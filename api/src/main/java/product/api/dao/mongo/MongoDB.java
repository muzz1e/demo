package product.api.dao.mongo;

import com.mongodb.*;
import product.api.entity.Product;

import java.net.UnknownHostException;

public class MongoDB {

    private static final String HOST = "localhost";
    private static final int PORT = 27017;
    private static final String DBNAME = "mongo";
    private static final String LOGIN = "";
    private static final String PASSWORD = "";
    private static final String TABLE = "product";

    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает 
    // возможность аутентифицироваться в MongoDB
    private  DB db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать 
    // с коллекциями / таблицами MongoDB
    private  DBCollection table;

    public  DBCollection countersCollection;

    public MongoDB() {
        try {
            // Создаем подключение
            mongoClient = new MongoClient(HOST, PORT);

            // Выбираем БД для дальнейшей работы
            db = mongoClient.getDB(DBNAME);

            // Входим под созданным логином и паролем
            authenticate = db.authenticate(LOGIN, PASSWORD.toCharArray());

            // Выбираем коллекцию/таблицу для дальнейшей работы
            table = db.getCollection(TABLE);
        } catch (UnknownHostException e) {
            // Если возникли проблемы при подключении сообщаем об этом
            System.err.println("Don't connect!");
        }
    }

    public  void addProduct(Product product){
        BasicDBObject document = new BasicDBObject();

        document.put("_id", getNextId("productid"));
        document.put("_name", product.getName());

        table.insert(document);
    }

    public int getProductIdByName(String name){
        BasicDBObject query = new BasicDBObject();

        // задаем поле и значение поля по которому будем искать
        query.put("_name", name);

        // осуществляем поиск
        DBObject result = table.findOne(query);
       return  Integer.parseInt((String) result.get("_id"));

    }


    private  String getNextId(String seq_name) {
        String sequence_collection = "seq"; // the name of the sequence collection
        String sequence_field = "seq"; // the name of the field which holds the sequence

        DBCollection seq = db.getCollection(sequence_collection); // get the collection (this will create it if needed)

        // this object represents your "query", its analogous to a WHERE clause in SQL
        DBObject query = new BasicDBObject();
        query.put("_id", seq_name); // where _id = the input sequence name

        // this object represents the "update" or the SET blah=blah in SQL
        DBObject change = new BasicDBObject(sequence_field, 1);
        DBObject update = new BasicDBObject("$inc", change); // the $inc here is a mongodb command for increment

        // Atomically updates the sequence field and returns the value for you
        DBObject res = seq.findAndModify(query, new BasicDBObject(), new BasicDBObject(), false, update, true, true);
        return res.get(sequence_field).toString();
    }
}
