package product.api.dao;

import org.springframework.stereotype.Repository;
import product.api.dao.mongo.MongoDB;
import product.api.entity.Product;

@Repository
public class ProductDaoMongo implements  ProductDao  {

    MongoDB mongoDB = new MongoDB();

    @Override
    public void addNewProduct(Product product) {
        mongoDB.addProduct(product);
    }

    @Override
    public int getProductIdByName(Product product) {
        return mongoDB.getProductIdByName(product.getName());
    }
}
