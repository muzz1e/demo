package product.api.dao;

import product.api.entity.Product;

public interface ProductDao {

    void addNewProduct(Product product);
    int getProductIdByName(Product product);

}
