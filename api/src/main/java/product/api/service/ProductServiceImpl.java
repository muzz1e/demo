package product.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import product.api.dao.ProductDaoMongo;
import product.api.entity.Product;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDaoMongo productDaoMongo;

    @Autowired
    Environment env;

    @Override
    public void addNewProduct(Product product) {
        productDaoMongo.addNewProduct(product);
    }

    @Override
    public int getProductIdByName(Product product) {
        return productDaoMongo.getProductIdByName(product);
    }

    @Override
    public boolean validatePrice(Product product, double avg) {
        //0.3 * avg < price < 1.7 * avg (Проверяем попадает ли цена в допустимый диапозон)
        double avgCheck = Double.parseDouble(env.getProperty("avg"));
        return ((1-avgCheck) * avg) < product.getPrice() && product.getPrice() < ((1+avgCheck) * avg);
    }

}
