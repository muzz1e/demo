package product.api.service;


import product.api.entity.Product;

public interface ProductService {

    void addNewProduct (Product product);
    int getProductIdByName(Product product);
    boolean validatePrice(Product product, double avg);
}
